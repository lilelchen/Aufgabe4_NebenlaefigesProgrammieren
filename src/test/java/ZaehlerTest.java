
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Diese Testklasse prueft den Zaehler.
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 */
public class ZaehlerTest {

    @Test
    public void testRun() throws Exception {
    	
    	//Es wird ein neuer speicherMock erstellt
        SpeicherMock speicherMock = new SpeicherMock();

        // Der speicherMock zaehlt von 10 bis 100 (Achtung: inkl. 100)
        Zaehler zaehler = new Zaehler(speicherMock, 10, 100);

        synchronized (speicherMock) {
            zaehler.start();

            /* Mithilfe einer For-Schleife wird so lange hochgez�hlt, bis der Counter von 0 her bei 91 angekommen ist. 
             * Dabei wird jeweils der Counter um 1 erh�ht.
             */
            for(int counter = 0; counter < 91; counter++){
                speicherMock.wait();

                // Neue Array-List wird erstellt
                List<Integer> history = speicherMock.getHistory();
                assertEquals(counter + 1, history.size());
                assertEquals(counter + 10, history.get(history.size() -1).intValue());

                System.out.println(history.get(history.size() -1).intValue());

                //Alle noch wartenden Threads werden informiert, dass dieser Thread abgearbeitet ist.
                speicherMock.notifyAll();
            }
        }
        
        // Die wartenden Threads werden in Reihenfolge abgearbeitet, nachdem der letzte Thread abgeschlossen wurde.
        zaehler.join();
    }

    // Negativer Test
    @Test
    public void testRunNegative() throws Exception {

        SpeicherMock speicherMock = new SpeicherMock();

        // Da der Zaehler von 100 bis 10 laeuft wird der Thread nie abgeschlossen.
        Zaehler zaehler = new Zaehler(speicherMock, 100, 10);
        zaehler.join();
        
        // Die erwartete Groesse entspricht deswegen 0.
        assertEquals(0, speicherMock.getHistory().size());
    }
}