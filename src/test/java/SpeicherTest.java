import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Diese Testklasse prueft den Speicher.
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 */


public class SpeicherTest {

	/*
	 * Erstellt einen neuen Speicher,
	 * setzt einen neuen Wert in den Speicher und testet ob die Methode isHatWert True zurueck gibt.
	 */
	@Test
	public void setWertTest() throws InterruptedException {
		Speicher sp = new Speicher();
		sp.setWert(15);
		assertTrue(sp.isHatWert());
		}
	
	/*
	 * Erstellt einen neuen Speicher,
	 * setzt einen neuen Wert in den Speicher und testet ob die Methode
	 * auch wirklich den eingegebenen Wert zurueck gibt.
	 */
	@Test
	public void getWertTest() throws InterruptedException{
		Speicher sp = new Speicher();
		sp.setWert(12);
		assertEquals(sp.getWert(), 12);
	}
	
	/*
	 * Erstellt einen neuen Speicher,
	 * setzt einen neuen Wert in den Speicher und testet ob die Methode isHatWert True zurueck gibt.
	 */
	@Test
	public void isHatWertTest() throws InterruptedException {
		
		Speicher sp = new Speicher();
		sp.setWert(9);
		assertTrue(sp.isHatWert());
	}
	
}