
import java.util.ArrayList;
import java.util.List;

/**
 * Mithilfe der Mock-Klasse kann der Zaehler getestet werden.
 *  
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 */


public class SpeicherMock implements SpeicherIf {
	
	private List<Integer> history;
	
	//Neue Arraylist wird erstellt
	public SpeicherMock() {
        history = new ArrayList<>();
    }

	
	//Diese Methode wird nicht benoetigt, deswegen return 0.
    @Override
    public int getWert() throws InterruptedException {
        return 0;
    }

    // Diese Methode setzt den Wert und f�llt ihn in die Arraylist.
    @Override
    public void setWert(int wert) throws InterruptedException {
    	
    	//Umwandeln von int in Integer und einfuellen in die Arraylist
        history.add(Integer.valueOf(wert));
    }

    //Diese Methode wird hier nicht benoetigt, deswegen return false.
    @Override
    public boolean isHatWert() {
        return false;
    }

    /**
     * @return history, der Arraylist
     */
    
    public List<Integer> getHistory() {
        return history;
    }
  }