/**
 * Die Klasse Drucker gibt die Zahl, die aktuell im Speicher ist, zurueck.
 * 
 * @author Stephanie Kaswurm, Anna-Lena Klaus
 */

public class Drucker extends Thread {
    private Speicher speicher;

    Drucker(Speicher s) {
        this.speicher = s;
    }


    /*
	 * Holt einen Wert vom Zaehler und gibt ihn aus, gefolgt von einem einzelnen
	 * Leerzeichen.
	 */
    @Override
    public void run() {
        try {

            synchronized (speicher){
                if(!speicher.isHatWert()){
                    speicher.wait();
                }
            }
            
            while (true) {
                synchronized (speicher){
                    System.out.print(speicher.getWert() + " ");

                    speicher.notifyAll();
                    speicher.wait();
                }
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

}