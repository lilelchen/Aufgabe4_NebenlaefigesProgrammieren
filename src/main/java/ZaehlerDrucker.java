
/**
 * Der Aufruf benoetigt zwei Parameter min und max - der Zaehler beginnt bei min
 * zu zaehlen und terminiert bei max.
 * 
 * @author Stephnie Kaswurm und Anna-Lena Klaus
 */

public class ZaehlerDrucker {
	
	/*
	 * Die Main-Methode erstellt einen neuen Speicher, Drucker und Zaehler.
	 * Danach startet sie den Thread Zaehler und anschliessend den Thread Drucker.
	 * Dies ist jedoch zufaellig, es koennte auch vorkommen das der Drucker-Thread
	 * schneller ist als der Zaehler-Thread.
	 * 
	 */
	

    public static void main(String[] args) throws InterruptedException {
    	
    	// Es muessen mindestens 2 Zahlen mitgegeben werden.
        if (args.length != 2) {
            System.out.println("Usage: ZaehlerDrucker <min> <max>");
            System.exit(1);
        }

        Speicher s = new Speicher();
        Drucker d = new Drucker(s);
        Zaehler z = new Zaehler(s, Integer.parseInt(args[0]), Integer.parseInt(args[1]));

        z.start();
        d.start();

        // bissi warten, damit der Test funktioniert
        Thread.sleep(5000);

    }

}